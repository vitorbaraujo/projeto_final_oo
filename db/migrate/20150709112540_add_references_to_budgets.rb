class AddReferencesToBudgets < ActiveRecord::Migration
  def change
    add_reference :budgets, :user, index: true, foreign_key: true
    add_index :budgets, [:user_id, :created_at]
  end
end
